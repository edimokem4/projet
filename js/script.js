document.addEventListener('DOMContentLoaded', function () {
    var myChart = Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Events over the year'
        },
        xAxis: {
            categories: ['Jan','Feb','Mar','Apr','May','Jun','Jul'
            ,'Aug','Sep','Oct','Nov','Dec']
        },
        yAxis: {
            title: {
                text: 'Events Organised'
            }
        },
        series: [{
            name: 'Jane',
            data: [45,22,54,65,1,11,14,14,14,12,18,9]
        }, {  
            name: 'Association_2',
               data: [45,2,5,5,1,411,4,14,1,12,18,19]
           }]
    });
});

document.addEventListener('DOMContentLoaded', function () {
    var myChart = Highcharts.chart('container2', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Events over the year'
        },
        xAxis: {
            categories: ['Jan','Feb','Mar','Apr','May','Jun','Jul'
            ,'Aug','Sep','Oct','Nov','Dec']
        },
        yAxis: {
            title: {
                text: 'Events Organised'
            }
        },
        series: [{
            name: 'Jane',
            data: [45,22,54,65,1,11,14,14,14,12,18,9]
        }, {  
            name: 'Association_2',
               data: [45,2,5,5,1,411,4,14,1,12,18,19]
           }]
    });
});


