(function ($) {

    function getParameterByName(valeur, url) {
        if (!url)
            url = window.location.href;
        valeur = valeur.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + valeur + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
        if (!results)
            return null;
        if (!results[2])
            return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    $.ajax({
        url: chemins.getEventById + "/" + getParameterByName('IDEVENT'),
        type: 'GET',
        success: function (reponse) {
            //console.log(reponse);
            var Event = $('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>')
                .append($('<div></div>').attr('class', 'tile')
                    .append($('<div></div>').attr('class', 'wrapper')
                        .append($('<div></div>').attr('class', 'header').html(reponse.titre))
                        .append($('<div></div>').attr('class', 'banner-img')
                            .append($('<img class="logo_event"/>').attr('src', reponse.logo)))
                        .append($('<div></div>').attr('class', 'dates')
                            .append($('<div></div>').attr('class', 'start')
                                .append($('<strong></strong').html('START'))
                                .append($('<p></p>').html(moment(new Date(reponse.start)).format('MMM DD, YYYY HH:mm'))))
                            .append($('<div></div>').attr('class', 'ends')
                                .append($('<strong style="color: red;"></strong').html('END'))
                                .append($('<p style="color: red;"></p>').html(moment(new Date(reponse.end)).format('MMM DD, YYYY HH:mm')))
                            )
                        )
                        .append($('<div></div>').attr('class', 'stats')
                            .append($('<div></div>')
                                .append($('<strong></strong').html('INVITED'))
                                .append($('<p></p>').html(reponse.invites)))
                            .append($('<div></div>')
                                .append($('<strong></strong').html('JOINED'))
                                .append($('<p></p>').html(reponse.joined)))
                            .append($('<div></div>')
                                .append($('<strong></strong').html('DECLINED'))
                                .append($('<p></p>').html(reponse.declined))
                            )
                        )
                        .append($('<div></div>').attr('class', 'footer')
                            .append($('<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>')
                                .append($('<i></i>').attr('class', 'fa fa-plus-circle t-3').html('Add'))
                            )
                        )
                    )
                );
            $('.rows').append(Event);
        },
        error: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR) },
        async: true
    });

    $('#dtable').DataTable({
        ajax: {
            url: chemins.getEventParticipants + '/' + getParameterByName('IDEVENT'),
            type: 'GET',
            dataSrc: 'data'
        },
        columns: [{ data: 'id' }, { data: 'participant' }, { data: 'sign' }]
    });

    $('#create').on('click', function () {
        var user = {
            IDEVENT: getParameterByName('IDEVENT'),
            NOM: $('#nom').val(),
            PRENOM: $('#prenom').val(),
            PHONE: $('#tel').val(),
            MAIL: $('#mail').val(),
            SEXE: $('#sexe').val(),
            CNI: $('#cni').val()
        };
        if (user.NOM !== '' || user.PRENOM !== '' || user.PHONE !== '' || user.MAIL !== '' || user.CNI !== '') {
            $.ajax({
                type: 'POST',
                url: chemins.addParticipant,
                data: user,
                success: function (data) {
                    console.log(data);
                    if (data.inserted) {
                        alert('Record successfully added');
                        M.toast({ html: 'Veuillez remplir tous les champs!!!!', classes: 'rounded' });
                        window.location.reload();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR) },
                async: true
            });
        } else {
            M.toast({ html: 'Veuillez remplir tous les champs!!!!', classes: 'rounded' });
        }
    });

})(jQuery);