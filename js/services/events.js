(function ($) {

    var Card = {
        container: null,
        initialize: function (id, titre, description, debut, fin, photo) {
            this.container = $('<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12"></div>').attr('id', id)
                .append($('<div></div>').attr('class', 'tile')
                    .append($('<div></div>').attr('class', 'wrapper')
                        .append($('<div></div>').attr('class', 'header').html(titre))
                        .append($('<div></div>').attr('class', 'banner-img')
                            .append($('<img />').attr('src', photo)))
                        .append($('<div></div>').attr('class', 'dates')
                            .append($('<div></div>').attr('class', 'start')
                                .append($('<strong></strong').html('START'))
                                .append($('<p></p>').html(moment(new Date(debut)).format('MMM DD, YYYY HH:mm'))))
                            .append($('<div></div>').attr('class', 'ends')
                                .append($('<strong style="color: red;"></strong').html('END'))
                                .append($('<p style="color: red;"></p>').html(moment(new Date(fin)).format('MMM DD, YYYY HH:mm')))
                            )
                        )
                    )
                );
            this.container.on('click', function () {
                window.location = 'liste.html?IDEVENT=' + id;
            });
        },
        bind: function () {
            $('.row').append(this.container);
        }
    };

    function loadEvents() {
        $.ajax({
            url: chemins.getEventsList,
            type: 'GET',
            success: function (reponse) {
                //console.log(reponse.eventsList);
                for (var i = 0; i < reponse.eventsList.length; i++) {
                    Card.initialize(reponse.eventsList[i].id, reponse.eventsList[i].titre, reponse.eventsList[i].description, reponse.eventsList[i].start, reponse.eventsList[i].end, reponse.eventsList[i].logo);
                    Card.bind();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR) },
            async: true
        });

        /**
         * rechargement automatique
         */
        setTimeout(() => {
            $('.row').empty();
            loadEvents();
        }, 200000);
    }

    loadEvents();

    $('#fan').on('click', function () {
        $.post(chemins.recherche, { KEY: $('#search').val() }, function (data) {
            console.log(data);
            if (data.length !== 0) {
                $('.row').empty();
                for (var i = 0; i < data.length; i++) {
                    Card.initialize(data[i].id, data[i].titre, data[i].description, data[i].start, data[i].end, data[i].logo);
                    Card.bind();
                }
                //alert(Object.values(data));
            } else {
                alert('No record found!!!');

                // 'rounded' is the class I'm applying to the toast
                // M.toast({ html: 'No record found!!!!', classes: 'rounded' });
            }
        });
    });

    $('#form').submit(function (e) {

        e.preventDefault();
        var $form = $(this);
        //alert($('#form').serialize());
        // check if the input is valid
        //if (!$form.valid()) return false;
        $.ajax({
            type: 'POST',
            url: chemins.recherche,
            data: {KEY: $('#search').val()},
            success: function (data) {
                console.log(data);
                $('.row').empty();
                for (var i = 0; i < data.length; i++) {
                    Card.initialize(data[i].id, data[i].titre, data[i].description, data[i].start, data[i].end, data[i].logo);
                    Card.bind();
                }
            }
        });
    });

})(jQuery);