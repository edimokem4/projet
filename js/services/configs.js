const Host = "http://localhost:1234/";
const chemins = {
    getEventsList: Host + "serveur/events/getAllEvents",
    getEventById: Host + "serveur/events/getEvent",
    getEventParticipants: Host + "serveur/events/getEventParticipants",
    recherche: Host + "serveur/events/fanEvent",
    addParticipant: Host + "serveur/events/addInvite",
    presence: Host + "serveur/events/checkParticipant"
}